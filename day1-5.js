// Menghitung tagihan
let tagihan = "100000";
tagihan = +tagihan;

if (tagihan <= 300000 && tagihan >= 50000 && typeof tagihan == "number") {
  let tip = tagihan * (15 / 100);
  console.log(`Rp. ${tip + tagihan}`);
} else {
  let tip = tagihan * (20 / 100);
  console.log(`Rp. ${tip + tagihan}`);
}

// Membandingkan Score
let scoreDolphins = 50;
let scoreKoalas = 25;

if (scoreDolphins > scoreKoalas && scoreDolphins >= 50) {
  console.log("Dolphins Memenangkan kejuaraan");
} else if (scoreKoalas > scoreDolphins && scoreKoalas >= 50) {
  console.log("Koalas memenangkan Kejuaraan");
} else if (
  scoreDolphins === scoreKoalas ||
  (scoreDolphins >= 50 && scoreKoalas >= 50)
) {
  console.log("Keduanya memenangkan Kejuaraan");
} else {
  console.log("tidak ada yang memenangkan Kejuaraan");
}

// Membandingkan BMI
let BeratMark = 54,
  TinggiMark = 1.65;
let BeratJhon = 56,
  TinggiJhon = 1.69;
let BMIMark = BeratMark / TinggiMark ** 2;
let BMIJohn = BeratJhon / TinggiJhon ** 2;

if (BMIMark != BMIJohn) {
  BMIMark > BMIJohn
    ? console.log(
        `BMI Mark (${BMIMark.toFixed(
          2
        )}) lebih besar daripada BMI Jhon ((${BMIJohn.toFixed(2)}))`
      )
    : console.log(
        `BMI Mark (${BMIMark.toFixed(
          2
        )}) lebih besar daripada BMI Jhon ((${BMIJohn.toFixed(2)}))`
      );
} else {
  console.log("BMI Mark dan BMI John sama");
}

let arr = [15, 70, 89, 50, 55, 57, 22, 23, 66, 77];
const sortArr = arr.sort();
sortArr.forEach((number) => {
  if (number > 70) {
    console.log(number);
  }
});

let nilai = 105;

if (nilai >= 75 && nilai < 100) {
  console.log("Lulus");
} else if (nilai < 0 || nilai > 100) {
  console.log("Nilai tidak valid");
} else {
  console.log("Tidak lulus");
}

function test(number) {
  if (number >= 75 && number <= 100) {
    console.log("Lulus");
  } else if (number < 0 || number > 100) {
    console.log("Nilai tidak valid");
  } else {
    console.log("Tidak lulus");
  }
}

test(75);

// const test2 = function (number){
//   if (number >= 75){
//     return "Lulus"
//   } else {
//     return "Tidak lulus"
//   }
// }

// const test3 = (number) => {
//   if (number > 75){
//     return "Lulus"
//   } else {
//     return "Tidak lulus"
//   }
// }

// let palindrom = ['a','b','c','b','a']

// let length = palindrom.length
// for (let id = 0; id < palindrom.length; id++ ) {
//   if (palindrom[id] != palindrom[length - (id + 1)]) {
//     console.log("bukan palindrom")
//     break;
//   }
//   else if (id == length - 1) {
//     console.log("palindrom")
//   }
// }

// let id2 = 0;
// while(id2 < palindrom.length) {
//   if (palindrom[id2] != palindrom[length - (id2 + 1)]) {
//     console.log("bukan palindrom")
//     break;
//   }
//   else if (id2 == length - 1) {
//     console.log("palindrom")
//   }
//   id2++
// }

// console.log(test(5));

// function test(number) {
//   return number + number;
// }

// const test = function (number) {
//   return number + number;
// };

// const test = (number) => {
//   return number + number;
// };

const nilai = 75;

// if (nilai >= 75) {
//   console.log("Lulus");
// } else {
//   console.log("Tidak lulus");
// }

// function test(number) {
//   if (number >= 75) {
//     return "Lulus";
//   } else {
//     return "Tidak lulus";
//   }
// }

// console.log(test(70));

// const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// for (let i = 0; i < arr.length; i++) {
//   if (arr[i] % 2 === 0) {
//     console.log(`bilangan ${arr[i]} merupakan bilangan genap`);
//   } else {
//     console.log(`bilangan ${arr[i]} merupakan bilangan ganjil`);
//   }
// }

// const kata = "ibu",
//   panjangKata = kata.length;

// let cek = true;

// for (let i = 0; i < panjangKata / 2; i++) {
//   if (kata[i] !== kata[panjangKata - 1 - i]) cek = false;
// }

// if (cek == true) console.log("palindrome");
// else console.log("bukan palindrome");

const identitas = [
  {
    nama: "Axel Berkati",
    umur: "20",
    tanggalLahir: "23 Oktober 2001",
    kampus: "Universitas Palangka Raya",
    domisili: "Kalimantan Tengah",
    Hobby: "Main game",
  },
  {
    nama: "Bayu Cucan Herdia",
    umur: "20",
    tanggalLahir: "27 Maret 2001",
    kampus: "Universitas Pasundan",
    domisili: "Bandung",
    Hobby: "Main game",
  },
  {
    nama: "Abrar Saskara",
    umur: "21",
    tanggalLahir: "13 September 2000",
    kampus: "Institut Teknologi Indonesia",
    domisili: "Tangerang Selatan",
    Hobby: "Main game",
  },
  {
    nama: "Ridhagama Islamiyah Nud'adha",
    umur: "20",
    tanggalLahir: "20 Maret 2001",
    kampus: "Universitas Bengkulu",
    domisili: "Bengkulu",
    Hobby: "Futsal",
  },
  {
    nama: "Dzun Nurroin",
    umur: "21",
    tanggalLahir: "13 November 2000",
    kampus: "Universitas Nurul Jadid",
    domisili: "Probolinggo",
    Hobby: "Tenis Meja",
  },
];

for (let index = 0; index < identitas.length; index++) {
  if (identitas[index].domisili == "Bengkulu") {
    console.log(identitas[index].domisili);
  }
}
